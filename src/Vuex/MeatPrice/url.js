const mainUrl = 'http://139.162.234.147:5015'

export const MeatPriceUrls = {
  postMeatPrice: mainUrl + '/meat/price/new',
  postStatusActive: mainUrl + '/MeatPrice/alter/active/status/',
  postStatusInactive: mainUrl + '/MeatPrice/alter/inactive/status/',
  getMeatPrices: mainUrl + '/meat/price/view/meat/',
  getMeatPriceMeatType: mainUrl + '/meat/price/view/meat/',
  deleteMeatPrice: mainUrl + '/meat/price/delete',
  editMeatPrice: mainUrl + '/MeatPrice/alter/',
  getAllMeatPrices: mainUrl + '/MeatPrice',
  getAllActiveMeatPrices: mainUrl + '/MeatPrice/active',
  getAllInactiveMeatPrices: mainUrl + '/MeatPrice/inactive',
  restoreMeatPrice: mainUrl + 'starts/restore/',
  getDeletedMeatPrice: mainUrl + 'MeatPrice/deleted'
}
