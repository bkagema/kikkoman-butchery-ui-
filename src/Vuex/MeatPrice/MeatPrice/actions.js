import Vue from 'vue'
import {MeatPriceUrls} from '../url'
import router from '../../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_meat_prices (context) {
    Vue.http.get(MeatPriceUrls.getAllMeatPrices).then(function (response) {
      context.commit('GET_ALL_MEAT_PRICES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_meat_prices (context, publicId) {
    Vue.http.get(MeatPriceUrls.getMeatPrices + publicId).then(function (response) {
      context.commit('GET_MEAT_PRICES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_meat_prices (context) {
    Vue.http.get(MeatPriceUrls.getDeletedMeatPrice).then(function (response) {
      context.commit('GET_DELETED_MEAT_PRICES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_meat_price (context, publicId) {
    Vue.http.get(MeatPriceUrls.getMeatPrice + publicId).then(function (response) {
      context.commit('GET_MEAT_PRICE', response.data)
      context.dispatch('loading_false')
    })
  },
  get_meat_price_meat_type (context, publicId) {
    Vue.http.get(MeatPriceUrls.getMeatPriceMeatType + publicId).then(function (response) {
      context.commit('GET_MEAT_PRICE_MEAT_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  get_meat_price_butchery (context, publicId) {
    Vue.http.get(MeatPriceUrls.getMeatPriceButchery + publicId).then(function (response) {
      context.commit('GET_MEAT_PRICE_BUTCHERY', response.data)
      context.dispatch('loading_false')
    })
  },
  post_meat_price (context, data) {
    context.errors = []
    const postData = {
      butchery: localStorage.getItem('butchery_id'),
      meat_type: data.meat_type_id,
      price: Number(data.price),
      session_id: localStorage.getItem('session_id')
    }
    Vue.http.post(MeatPriceUrls.postMeatPrice, postData).then(function (response) {
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: error.data.message})
      console.log(error.data)
    })
  },
  update_meat_price (context, data) {
    context.errors = []
    const postData = {
      meat_type: data.meat_type_id,
      price: Number(data.price)
    }
    Vue.http.post(MeatPriceUrls.postMeatPrice, postData).then(function (response) {
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: error.data.message})
      console.log(error.data)
    })
  },
  delete_meat_price (context, publicId) {
    Vue.http.get(MeatPriceUrls.deleteMeatPrice + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_MEAT_PRICES', response.data)
      router.push({
        name: 'Module.DeletedMeatPrices'
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restore_meat_price (context, publicId) {
    Vue.http.get(MeatPriceUrls.restoreMeatPrice + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_MEAT_PRICES', response.data)
      router.push({
        name: 'Module.MeatPrices'
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
