import mutations from './mutations'
import actions from './actions'

const state = {
  all_meat_prices: [],
  meat_prices: [],
  meat_price: [],
  meat_price_meat_type: [],
  meat_price_butchery: [],
  deleted_meat_prices: []
}

export default {
  state, mutations, actions
}
