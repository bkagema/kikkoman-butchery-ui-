export default {
  GET_MEAT_PRICES (state, data) {
    state.meat_prices = data.data[0]
  },
  GET_ALL_MEAT_PRICES (state, data) {
    state.all_meat_prices = data.data
  },
  GET_MEAT_PRICE_MEAT_TYPE (state, data) {
    state.meat_price_meat_type = data.data
  },
  GET_MEAT_PRICE (state, data) {
    state.meat_price = data.data
  },
  GET_MEAT_PRICE_BUTCHERY (state, data) {
    state.meat_price_butchery = data.data
  },
  GET_DELETED_MEAT_PRICES (state, data) {
    state.deleted_meat_prices = data.data
  }
}
