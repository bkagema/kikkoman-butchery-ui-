const mainUrl = 'http://139.162.234.147:5015'

export const MeatPurchaseUrls = {
  postMeatPurchase: mainUrl + '/meat/purchase/new',
  postStatusActive: mainUrl + '/tax/alter/active/status/',
  postStatusInactive: mainUrl + '/tax/alter/inactive/status/',
  getMeatPurchases: mainUrl + '/tax',
  getMeatPurchase: mainUrl + '/meat/purchase/view/',
  getMeatPurchaseMeatType: mainUrl + '/meat/purchase/view/meat/type/',
  getMeatPurchaseButchery: mainUrl + '/meat/purchase/view/butchery/',
  deleteMeatPurchase: mainUrl + '/meat/purchase/delete',
  editMeatPurchase: mainUrl + '/meat/purchase/modify',
  getAllMeatPurchases: mainUrl + '/meat/purchase/view/',
  getAllActiveTaxes: mainUrl + '/tax/active',
  getAllInactiveTaxes: mainUrl + '/tax/inactive',
  restoreMeatPurchase: mainUrl + 'starts/restore/',
  getDeletedMeatPurchase: mainUrl + 'tax/deleted'
}
