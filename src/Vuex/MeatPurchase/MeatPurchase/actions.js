import Vue from 'vue'
import axios from 'axios'
import {MeatPurchaseUrls} from '../url'
import router from '../../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_meat_purchases (context) {
    Vue.http.get(MeatPurchaseUrls.getAllMeatPurchases).then(function (response) {
      context.commit('GET_ALL_MEAT_PURCHASES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_meat_purchases (context) {
    Vue.http.get(MeatPurchaseUrls.getMeatPurchases).then(function (response) {
      context.commit('GET_MEAT_PURCHASES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_meat_purchases (context) {
    Vue.http.get(MeatPurchaseUrls.getDeletedMeatPurchase).then(function (response) {
      context.commit('GET_DELETED_MEAT_PURCHASES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_meat_purchase (context, publicId) {
    Vue.http.get(MeatPurchaseUrls.getMeatPurchase + publicId).then(function (response) {
      context.commit('GET_MEAT_PURCHASE', response.data)
      context.dispatch('loading_false')
    })
  },
  get_meat_purchase_butchery (context, publicId) {
    Vue.http.get(MeatPurchaseUrls.getMeatPurchaseButchery + publicId).then(function (response) {
      context.commit('GET_MEAT_PURCHASE_BUTCHERY', response.data)
      context.dispatch('loading_false')
    })
  },
  get_meat_purchase_meat_type (context, publicId) {
    Vue.http.get(MeatPurchaseUrls.getMeatPurchaseMeatType + publicId).then(function (response) {
      context.commit('GET_MEAT_PURCHASE_MEAT_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_meat_purchase (context, data) {
    context.errors = []
    const postData = {
      butchery: localStorage.getItem('butchery_id'),
      meat_type: data.meat_type_public_id,
      weight: Number(data.meat_purchase_weight),
      cost: Number(data.meat_purchase_cost),
      receipt: data.meat_purchase_receipt,
      supplier: data.meat_purchase_supplier,
      session_id: localStorage.getItem('session_id')
    }
    Vue.http.post(MeatPurchaseUrls.postMeatPurchase, postData).then(function (response) {
      context.dispatch('get_all_meat_purchases')
      router.push({
        name: 'AllMeatPurchases'
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: error.data.message})
      console.log(error.data)
    })
  },
  update_meat_purchase (context, data) {
    context.errors = []
    const postData = {
      butchery: localStorage.getItem('butchery_id'),
      meat_purchase_id: data.meat_purchase_public_id,
      weight: Number(data.meat_purchase_weight),
      cost: Number(data.meat_purchase_cost),
      meat_type: data.meat_type_id,
      session_id: localStorage.getItem('session_id')
    }
    axios.patch(MeatPurchaseUrls.editMeatPurchase, postData).then(function (response) {
      context.dispatch('get_all_meat_purchases')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    })
  },
  delete_meat_purchase (context, data) {
    axios.patch(MeatPurchaseUrls.deleteMeatPurchase, data).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_MEAT_PURCHASES', response.data)
      router.push({
        name: 'Dashboard'
      })
      context.dispatch('loading_false')
    })
  },
  restore_meat_purchase (context, publicId) {
    Vue.http.get(MeatPurchaseUrls.restoreMeatPurchase + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_MEAT_PURCHASES', response.data)
      router.push({
        name: 'Module.MeatPurchases'
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
