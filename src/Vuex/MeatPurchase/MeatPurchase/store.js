import mutations from './mutations'
import actions from './actions'

const state = {
  all_meat_purchases: [],
  meat_purchases: [],
  meat_purchase: [],
  meat_purchase_butchery: [],
  meat_purchase_meat_type: [],
  deleted_meat_purchases: []
}

export default {
  state, mutations, actions
}
