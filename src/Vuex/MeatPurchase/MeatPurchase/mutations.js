export default {
  GET_MEAT_PURCHASES (state, data) {
    state.meat_purchases = data.data
  },
  GET_ALL_MEAT_PURCHASES (state, data) {
    state.all_meat_purchases = data.data
  },
  GET_MEAT_PURCHASE (state, data) {
    state.meat_purchase = data.data[0]
  },
  GET_MEAT_PURCHASE_BUTCHERY (state, data) {
    state.meat_purchase_butchery = data.data[0]
  },
  GET_MEAT_PURCHASE_MEAT_TYPE (state, data) {
    state.meat_purchase_meat_type = data.data[0]
  },
  GET_DELETED_MEAT_PURCHASES (state, data) {
    state.deleted_meat_purchases = data.data
  }
}
