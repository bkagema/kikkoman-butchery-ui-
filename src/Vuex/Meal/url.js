const mainUrl = 'http://139.162.234.147:5015'

export const MealUrls = {
  postMeal: mainUrl + '/meal/new',
  postStatusActive: mainUrl + '/Meal/alter/active/status/',
  postStatusInactive: mainUrl + '/Meal/alter/inactive/status/',
  getMeal: mainUrl + '/meal/view/',
  getMealButchery: mainUrl + '/meal/view/butchery/',
  getMealMeatType: mainUrl + '/meat/price/view/meat/',
  deleteMeal: mainUrl + '/meal/delete',
  editMeal: mainUrl + '/Meal/alter/',
  getAllMeals: mainUrl + '/meal/view/',
  getAllActiveMeals: mainUrl + '/Meal/active',
  getAllInactiveMeals: mainUrl + '/Meal/inactive',
  restoreMeal: mainUrl + 'starts/restore/',
  getDeletedMeal: mainUrl + 'Meal/deleted'
}
