import mutations from './mutations'
import actions from './actions'

const state = {
  all_meals: [],
  meals: [],
  meal: [],
  meal_meat_type: [],
  meal_butchery: [],
  deleted_meals: []
}

export default {
  state, mutations, actions
}
