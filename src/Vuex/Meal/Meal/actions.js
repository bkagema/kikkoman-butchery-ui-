import Vue from 'vue'
import axios from 'axios'
import {MealUrls} from '../url'
import router from '../../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_meals (context) {
    Vue.http.get(MealUrls.getAllMeals).then(function (response) {
      context.commit('GET_ALL_MEALS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_meals (context, publicId) {
    Vue.http.get(MealUrls.getMeals + publicId).then(function (response) {
      context.commit('GET_MEALS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_meals (context) {
    Vue.http.get(MealUrls.getDeletedMeal).then(function (response) {
      context.commit('GET_DELETED_MEALS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_meal (context, publicId) {
    Vue.http.get(MealUrls.getMeal + publicId).then(function (response) {
      context.commit('GET_MEAL', response.data)
      context.dispatch('loading_false')
    })
  },
  get_meal_meat_type (context, publicId) {
    Vue.http.get(MealUrls.getMealMeatType + publicId).then(function (response) {
      context.commit('GET_meal_MEAT_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  get_meal_butchery (context, publicId) {
    Vue.http.get(MealUrls.getMealButchery + publicId).then(function (response) {
      context.commit('GET_MEAL_BUTCHERY', response.data)
      context.dispatch('loading_false')
    })
  },
  post_meal (context, data) {
    context.errors = []
    const postData = {
      butchery: localStorage.getItem('butchery_id'),
      name: data.meal_name,
      meat_type: data.meat_type_id,
      weight: Number(data.meal_meat_weight),
      quantity: Number(data.meal_sachet_quantity),
      price: Number(data.meal_price),
      session_id: localStorage.getItem('session_id')
    }
    Vue.http.post(MealUrls.postMeal, postData).then(function (response) {
      context.dispatch('get_all_meals')
      router.push({
        name: 'AllMenus'
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: error.data.message})
      console.log(error.data)
    })
  },
  update_meal (context, data) {
    // context.errors = []
    // const postData = {
    //   butchery: localStorage.getItem('butchery_id'),
    //   name: data.meal_name,
    //   meat_type: data.meat_type_id,
    //   weight: Number(data.meal_meat_weight),
    //   quantity: Number(data.meal_sachet_quantity),
    //   price: Number(data.meal_price),
    //   session_id: localStorage.getItem('session_id')
    // }
    Vue.http.post(MealUrls.postMeal, data.meal_public_id).then(function (response) {
      context.dispatch('get_all_meals')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: error.data.message})
      console.log(error.data)
    })
  },
  delete_meal (context, data) {
    axios.patch(MealUrls.deleteMeal, data).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_MEALS', response.data)
      router.push({
        name: 'Dashboard'
      })
      context.dispatch('loading_false')
    })
  },
  restore_meal (context, publicId) {
    Vue.http.get(MealUrls.restoreMeal + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_MEALS', response.data)
      router.push({
        name: 'Module.Meals'
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
