export default {
  GET_MEAL (state, data) {
    state.meal = data.data[0]
  },
  GET_ALL_MEALS (state, data) {
    state.all_meals = data.data
  },
  GET_meal_MEAT_TYPE (state, data) {
    state.meal_meat_type = data.data
  },
  GET_MEAL_BUTCHERY (state, data) {
    state.meal_butchery = data.data[0]
  },
  GET_DELETED_MEALS (state, data) {
    state.deleted_meals = data.data[0]
  }
}
