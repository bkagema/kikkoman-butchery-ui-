const mainUrl = 'http://139.162.234.147:5015'

export const MeatSaleUrls = {
  postMeatSale: mainUrl + '/meat/sale/new',
  postStatusActive: mainUrl + '/MeatSale/alter/active/status/',
  postStatusInactive: mainUrl + '/MeatSale/alter/inactive/status/',
  getMeatSales: mainUrl + '/MeatSale',
  getMeatSale: mainUrl + '/meat/sale/view/',
  getMeatSaleButchery: mainUrl + '/meat/sale/view/butchery/',
  deleteMeatSale: mainUrl + '/MeatSale/delete/',
  editMeatSale: mainUrl + '/MeatSale/alter/',
  getAllMeatSales: mainUrl + '/meat/sale/view/',
  getAllActiveMeatSalees: mainUrl + '/MeatSale/active',
  getAllInactiveMeatSales: mainUrl + '/MeatSale/inactive',
  restoreMeatSale: mainUrl + 'starts/restore/',
  getDeletedMeatSale: mainUrl + 'MeatSale/deleted'
}
