import mutations from './mutations'
import actions from './actions'

const state = {
  all_meat_sales: [],
  meat_sales: [],
  meat_sale: [],
  meat_sale_butchery: [],
  deleted_meat_sales: []
}

export default {
  state, mutations, actions
}
