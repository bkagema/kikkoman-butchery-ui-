import Vue from 'vue'
import {MeatSaleUrls} from '../url'
import router from '../../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_meat_sales (context) {
    Vue.http.get(MeatSaleUrls.getAllMeatSales).then(function (response) {
      context.commit('GET_ALL_MEAT_SALES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_meat_sales (context) {
    Vue.http.get(MeatSaleUrls.getMeatSales).then(function (response) {
      context.commit('GET_MEAT_SALES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_meat_sales (context) {
    Vue.http.get(MeatSaleUrls.getDeletedMeatSale).then(function (response) {
      context.commit('GET_DELETED_MEAT_SALES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_meat_sale (context, publicId) {
    Vue.http.get(MeatSaleUrls.getMeatSale + publicId).then(function (response) {
      context.commit('GET_MEAT_SALE', response.data)
      context.dispatch('loading_false')
    })
  },
  get_meat_sale_butchery (context, publicId) {
    Vue.http.get(MeatSaleUrls.getMeatSaleButchery + publicId).then(function (response) {
      context.commit('GET_MEAT_SALE_BUTCHERY', response.data)
      context.dispatch('loading_false')
    })
  },
  post_meat_sale (context, data) {
    context.errors = []
    const postData = {
      butchery: localStorage.getItem('butchery_id'),
      meat_type: data.meat_type_id,
      weight: Number(data.weight),
      price_id: data.meat_selling_price,
      session_id: localStorage.getItem('session_id')
    }
    Vue.http.post(MeatSaleUrls.postMeatSale, postData).then(function (response) {
      context.dispatch('get_all_meat_sales')
      router.push({
        name: 'AllMeatSales'
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: error.data.message})
      console.log(error.data)
    })
  },
  update_meat_sale (context, data) {
    context.errors = []
    const postData = {
      name: data.name,
      description: data.description
    }
    Vue.http.post(MeatSaleUrls.postMeatSale, postData).then(function (response) {
      context.dispatch('get_all_meat_sales')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: error.data.message})
      console.log(error.data)
    })
  },
  delete_meat_sale (context, publicId) {
    Vue.http.get(MeatSaleUrls.deleteMeatSale + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_MEAT_SALES', response.data)
      router.push({
        name: 'Module.DeletedMeatSales'
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restore_meat_sale (context, publicId) {
    Vue.http.get(MeatSaleUrls.restoreMeatSale + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_MEAT_SALES', response.data)
      router.push({
        name: 'Module.Meat Sales'
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
