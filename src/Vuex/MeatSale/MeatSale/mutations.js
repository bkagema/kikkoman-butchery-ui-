export default {
  GET_MEAT_SALES (state, data) {
    state.meat_sales = data.data
  },
  GET_ALL_MEAT_SALES (state, data) {
    state.all_meat_sales = data.data
  },
  GET_MEAT_SALE (state, data) {
    state.meat_sale = data.data[0]
  },
  GET_MEAT_SALE_BUTCHERY (state, data) {
    state.meat_sale_butchery = data.data[0]
  },
  GET_DELETED_MEAT_SALES (state, data) {
    state.deleted_meat_sales = data.data
  }
}
