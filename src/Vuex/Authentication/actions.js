import axios from 'axios'
import {AuthenticationUrls} from '../default/url'
import router from '../../router/index'

export default {
  get_authentication (context, publicId) {
    axios.get(AuthenticationUrls.getAuthentication + publicId).then(function (response) {
      context.commit('GET_AUTHENTICATION', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_AUTHENTICATION', data)
      context.dispatch('loading_false')
    })
  },
  post_authentication (context, data) {
    axios.post(AuthenticationUrls.postAuthentication, data).then(function (response) {
      context.dispatch('get_all_authentication')
      // console.log(response.data.auth_token)
      let accessToken = 'access_token'
      localStorage.setItem(accessToken, 'access_token')
      let sessionId = 'session_id'
      localStorage.setItem(sessionId, 'W0tas3232yTGZQ2ltpJ9xua32yR1')
      let butcheryId = 'butchery_id'
      localStorage.setItem(butcheryId, 'd59c2a05-f8e0-44f7-8e5d-e5801b352337')
      localStorage.setItem('access_token', response.data.access_token)
      router.push({
        name: 'Dashboard'
      })
      context.dispatch('loading_false')
    })
  },
  get_all_authentication (context, data) {
    axios.get(AuthenticationUrls.getAuthenticatedUser, data).then(function (response) {
      localStorage.setItem('access_token', response.data.access_token)
      localStorage.setItem('session_id', response.data.data[0].session_id)
      localStorage.setItem('butchery_id', response.data.data[0].butchery_id)
      localStorage.setItem('full_name', response.data.data.full_name)
      // if (response.data.data[0].client_secret === 'Client') {
      //   router.push({
      //     name: data.redirect_url
      //   })
      // }
      context.dispatch('loading_false')
    })
  },
  update_authentication (context, data) {
    axios.patch(AuthenticationUrls.editAuthentication, data).then(function (response) {
      context.dispatch('get_all_authentication')
      router.push({
        name: 'Dashboard'
      })
      context.dispatch('loading_false')
    })
  }
}
