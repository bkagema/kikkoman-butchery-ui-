import mutations from './mutations'
import actions from './actions'

const state = {
  all_authentication: [],
  authentication: [],
  deleted_authentication: []
}

export default {
  state, mutations, actions
}
