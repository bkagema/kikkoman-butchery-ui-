export default {
  GET_AUTHENTICATION (state, data) {
    state.authentication = data.data
  },
  GET_ALL_AUTHENTICATION (state, data) {
    state.all_authentication = data.data
  },
  GET_DELETED_AUTHENTICATION (state, data) {
    state.deleted_authentication = data.data
  }
}
