import Vue from 'vue'
import Vuex from 'vuex'
import MeatTypeStore from './MeatTypes/MeatTypes/store'
import MeatPurchaseStore from './MeatPurchase/MeatPurchase/store'
import MeatSaleStore from './MeatSale/MeatSale/store'
import MeatPriceStore from './MeatPrice/MeatPrice/store'
import MealStore from './Meal/Meal/store'
import AuthenticationStore from './Authentication/store'

Vue.config.productionTip = false
Vue.use(Vuex)
Vue.config.debug = true

const debug = process.env.NODE_ENV !== 'production'

export const strict = false
export default new Vuex.Store({
  modules: {
    MeatTypeStore,
    MeatPurchaseStore,
    MeatSaleStore,
    MeatPriceStore,
    MealStore,
    AuthenticationStore
  },
  strict: debug
})
