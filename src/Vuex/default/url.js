// If there is an external url file uncomment below and replace main url with the import
// import url from ''

const aumraUrl = 'http://aumra.keatrading.co.ke/oauth/token'

export const AuthenticationUrls = {
  postAuthentication: aumraUrl + '',
  getAllAuthentication: aumraUrl + '',
  getAuthentication: aumraUrl + '',
  editAuthentication: aumraUrl + '',
  getDeletedAuthentication: aumraUrl + '',
  deleteAuthentication: aumraUrl + ''
}
