import mutations from './mutations'
import actions from './actions'

const state = {
  all_meat_types: [],
  meat_types: [],
  meat_type: [],
  deleted_meat_types: [],
  approved_meat_types: []
}

export default {
  state, mutations, actions
}
