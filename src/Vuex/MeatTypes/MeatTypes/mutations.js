export default {
  GET_MEAT_TYPES (state, data) {
    state.meat_types = data.data[0]
  },
  GET_ALL_MEAT_TYPES (state, data) {
    state.all_meat_types = data.data
  },
  GET_MEAT_TYPE (state, data) {
    state.meat_type = data.data[0]
  },
  GET_DELETED_MEAT_TYPES (state, data) {
    state.deleted_meat_types = data.data
  },
  GET_APPROVED_MEAT_TYPES (state, data) {
    state.approved_meat_types = data.data
  }
}
