import Vue from 'vue'
import axios from 'axios'
import {MeatTypeUrls} from '../url'
import router from '../../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_meat_types (context) {
    Vue.http.get(MeatTypeUrls.getAllMeatTypes).then(function (response) {
      context.commit('GET_ALL_MEAT_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_meat_types (context, publicId) {
    Vue.http.get(MeatTypeUrls.getMeatTypes + publicId).then(function (response) {
      context.commit('GET_MEAT_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_approved_meat_types (context) {
    Vue.http.get(MeatTypeUrls.getApprovedMeatType).then(function (response) {
      context.commit('GET_APPROVED_MEAT_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_meat_types (context) {
    Vue.http.get(MeatTypeUrls.getDeletedMeatType).then(function (response) {
      context.commit('GET_DELETED_MEAT_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_meat_type (context, publicId) {
    Vue.http.get(MeatTypeUrls.getMeatType + publicId).then(function (response) {
      context.commit('GET_MEAT_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_meat_type (context, data) {
    Vue.http.post(MeatTypeUrls.postMeatType, data).then(function (response) {
      context.dispatch('get_all_meat_types')
      router.push({
        name: 'AllMeatTypes'
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: error.data.message})
      console.log(error.data)
    })
  },
  update_meat_type (context, data) {
    context.errors = []
    const postData = {
      name: data.meat_type_name,
      meat_type_id: data.meat_type_public_id,
      session_id: localStorage.getItem('session_id')
    }
    axios.patch(MeatTypeUrls.editMeatType, postData).then(function (response) {
      context.dispatch('get_all_meat_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    })
  },
  delete_meat_type (context, data) {
    axios.patch(MeatTypeUrls.deleteMeatType, data).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_MEAT_TYPES', response.data)
      router.push({
        name: 'Dashboard'
      })
      context.dispatch('loading_false')
    })
  },
  restore_meat_type (context, publicId) {
    Vue.http.get(MeatTypeUrls.restoreMeatType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_MEAT_TYPES', response.data)
      router.push({
        name: 'Module.Meat Types'
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
