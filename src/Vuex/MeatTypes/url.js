const mainUrl = 'http://139.162.234.147:5015'

export const MeatTypeUrls = {
  postMeatType: mainUrl + '/meat/type/new',
  postStatusActive: mainUrl + '/tax/alter/active/status/',
  postStatusInactive: mainUrl + '/tax/alter/inactive/status/',
  getApprovedMeatType: mainUrl + '/meat/type/view/',
  getMeatType: mainUrl + '/meat/type/view/',
  getMeatTypes: mainUrl + '/meat/price/view/meat/',
  deleteMeatType: mainUrl + '/meat/type/delete',
  editMeatType: mainUrl + '/meat/type/modify',
  getAllTaxes: mainUrl + '/tax',
  getAllActiveTaxes: mainUrl + '/tax/active',
  getAllMeatTypes: mainUrl + '/meat/type/view/all/',
  restoreTax: mainUrl + 'starts/restore/',
  getDeletedTax: mainUrl + 'tax/deleted'
}
