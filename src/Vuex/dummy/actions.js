import Vue from 'vue'
import {startUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_starts (context) {
    Vue.http.get(startUrls.getAllStarts).then(function (response) {
      context.commit('GET_ALL_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_starts (context, publicId) {
    Vue.http.get(startUrls.getStarts + publicId).then(function (response) {
      context.commit('GET_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_starts (context) {
    Vue.http.get(startUrls.getDeletedStart).then(function (response) {
      context.commit('GET_DELETED_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_start (context, publicId) {
    Vue.http.get(startUrls.getStart + publicId).then(function (response) {
      context.commit('GET_START', response.data)
      context.dispatch('loading_false')
    })
  },
  post_start (context, data) {
    Vue.http.post(startUrls.postStart, data).then(function () {
      context.dispatch('get_all_starts')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_start (context, data) {
    Vue.http.post(startUrls.editStart, data).then(function () {
      context.dispatch('get_all_starts')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_start (context, publicId) {
    Vue.http.get(startUrls.deleteStart + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_STARTS', response.data)
      router.push({
        name: 'Module.DeletedStarts'
      })
      context.dispatch('loading_false')
    })
  },
  restoreStart (context, publicId) {
    Vue.http.get(startUrls.restoreStart + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_STARTS', response.data)
      router.push({
        name: 'Module.Starts'
      })
      context.dispatch('loading_false')
    })
  }
}
