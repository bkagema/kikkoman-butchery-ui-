export default {
  computed: {
    allMeatSales: function () {
      return this.$store.state.MeatSaleStore.all_meat_sales
    },
    SingleMeatSale: function () {
      return this.$store.state.MeatSaleStore.meat_sale
    },
    SingleMeatSaleButchery: function () {
      return this.$store.state.MeatSaleStore.meat_sale_butchery
    }
  }
}
