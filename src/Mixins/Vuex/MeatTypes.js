export default {
  computed: {
    allMeatTypes: function () {
      return this.$store.state.MeatTypeStore.all_meat_types
    },
    MeatTypes: function () {
      return this.$store.state.MeatTypeStore.meat_types
    },
    ApprovedMeatTypes: function () {
      return this.$store.state.MeatTypeStore.approved_meat_types
    },
    SingleMeatTypes: function () {
      return this.$store.state.MeatTypeStore.meat_type
    }
  }
}
