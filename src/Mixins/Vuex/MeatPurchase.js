export default {
  computed: {
    allMeatPurchases: function () {
      return this.$store.state.MeatPurchaseStore.all_meat_purchases
    },
    SingleMeatPurchases: function () {
      return this.$store.state.MeatPurchaseStore.meat_purchase
    },
    SingleMeatPurchaseButchery: function () {
      return this.$store.state.MeatPurchaseStore.meat_purchase_butchery
    },
    SingleMeatPurchaseMeatType: function () {
      return this.$store.state.MeatPurchaseStore.meat_purchase_meat_type
    }
  }
}
