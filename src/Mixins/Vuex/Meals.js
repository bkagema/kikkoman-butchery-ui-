export default {
  computed: {
    allMeals: function () {
      return this.$store.state.MealStore.all_meals
    },
    Meals: function () {
      return this.$store.state.MealStore.meat_prices
    },
    SingleMeal: function () {
      return this.$store.state.MealStore.meal
    },
    SingleMealButchery: function () {
      return this.$store.state.MealStore.meal_butchery
    },
    SingleMealMeatType: function () {
      return this.$store.state.MealStore.meat_price_meat_type
    }
  }
}
