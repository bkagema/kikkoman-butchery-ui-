export default {
  computed: {
    allMeatPrices: function () {
      return this.$store.state.MeatPriceStore.all_meat_prices
    },
    MeatPrices: function () {
      return this.$store.state.MeatPriceStore.meat_prices
    },
    SingleMeatPrice: function () {
      return this.$store.state.MeatPriceStore.meat_price
    },
    SingleMeatPriceButchery: function () {
      return this.$store.state.MeatPurchaseStore.meat_price_butchery
    },
    SingleMeatPriceMeatType: function () {
      return this.$store.state.MeatPurchaseStore.meat_price_meat_type
    }
  }
}
