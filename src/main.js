// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueGoodTable from 'vue-good-table'
import Vuex from 'vuex'
import VueResource from 'vue-resource'
import store from './Vuex/Store'
import axios from 'axios'
import moment from 'moment'
import VueMomentJS from 'vue-momentjs'
import Snotify from 'vue-snotify'
import VueNotifications from 'vue-notifications'
import miniToastr from 'mini-toastr'
import Datepicker from 'vuejs-datepicker'
import Vuetify from 'vuetify'
import VuexPersist from 'vuex-persist'
import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
import 'babel-polyfill'
import VCharts from 'v-charts'
import VueSessionStorage from 'vue-sessionstorage'
import 'bootstrap/dist/css/bootstrap.min.css'
import '@/assets/css/main.css'
// import VuexPersistence from 'vuex-persist'

const vuexPersist = new VuexPersist({
  key: 'my-app',
  storage: localStorage
})

// export default {
//   // ...
//   components: {
//     Datepicker
//   }
//   // ...
// }

// If using mini-toastr, provide additional configuration
const toastTypes = {
  success: 'success',
  error: 'error',
  info: 'info',
  warn: 'warn'
}

miniToastr.init({types: toastTypes})

// Here we setup messages output to `mini-toastr`
function toast ({title, message, type, timeout, cb}) {
  return miniToastr[type](message, title, timeout, cb)
}

// Binding for methods .success(), .error() and etc. You can specify and map your own methods here.
// Required to pipe our output to UI library (mini-toastr in example here)
// All not-specified events (types) would be piped to output in console.
const options = {
  success: toast,
  error: toast,
  info: toast,
  warn: toast
}

Vue.config.productionTip = false
Vue.use(VueSessionStorage)
Vue.use(Vuetify)
Vue.use(VueGoodTable)
Vue.use(Snotify)
Vue.use(VueMomentJS, moment)
Vue.use(VueGoodTable)
Vue.use(Vuex, [vuexPersist.plugin])
Vue.use(VueResource)
Vue.use(VueNotifications, options)// VueNotifications have auto install but if we want to specify options we've got to do it manually.
Vue.use(Datepicker)
Vue.use(VCharts)
Vue.config.productionTip = false

// Vue.use(VuexPersistence)
/* eslint-disable no-new */

// Add a request interceptor
axios.interceptors.request.use(function (config) {
  store.dispatch('loading_true')
  // Do something before request is sent
  config.responseType = 'application/json'
  if (config.url === 'http://aumra.keatrading.co.ke/oauth/token') {
    config.headers = {'Authorization': 'Bearer ' + localStorage.getItem('access_token')}
  }
  if ((config.method === 'post' || config.method === 'put' || config.method === 'patch') && localStorage.getItem('session_id') !== null) {
    config.data = Object.assign(config.data, {session_id: localStorage.getItem('session_id')})
  }
  return config
}, function (error) {
  return Promise.reject(error)
})

// Add a response interceptor
axios.interceptors.response.use(function (response) {
  store.dispatch('loading_false')
  // Do something with response data
  if (response.status < 400) {
    if (response.data.message) {
      VueNotifications.info({message: response.data.message})
    }
  }
  return response
}, function (error) {
  store.dispatch('loading_false')
  // Do something with response error
  if (error.hasOwnProperty('response')) {
    if (error.request.response.hasOwnProperty('message')) {
      VueNotifications.error({message: error.request.response.message})
    }
    if (error.request.response.hasOwnProperty('messages')) {
      for (var i = 0; error.request.response.messages.length > i; i++) {
        VueNotifications.error({message: error.request.response.messages[i]})
      }
    }
  }
  return Promise.reject(error)
})

Vue.use(Vuetify, {
  theme: {
    primary: '#FB8C00',
    secondary: '#b0bec5',
    accent: '#8c9eff'
  }
})

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
