import Vue from 'vue'
import Router from 'vue-router'
import Management from '@/components/Management'
import Dashboard from '@/components/Modules/Management/Pages/Dashboard'
import Login from '@/components/Modules/Management/Basics/Login'

// Management

// Meat Price
import NewMeatPriceSale from '@/components/Modules/Management/Pages/Meat Price/NewMeatPriceSale'

// Meat Purchase
import NewMeatPurchase from '@/components/Modules/Management/Pages/Meat Purchase/NewMeatPurchase'
import AllMeatPurchases from '@/components/Modules/Management/Pages/Meat Purchase/AllMeatPurchases'
import SingleMeatPurchase from '@/components/Modules/Management/Pages/Meat Purchase/SingleMeatPurchase'
import SingleMeatPurchaseButchery from '@/components/Modules/Management/Pages/Meat Purchase/SingleMeatPurchaseButchery'
import SingleMeatPurchaseMeatType from '@/components/Modules/Management/Pages/Meat Purchase/SingleMeatPurchaseMeatType'
import MeatPurchaseSettings from '@/components/Modules/Management/Pages/Meat Purchase/MeatPurchaseSettings'

// Meat Sales
import NewMeatSales from '@/components/Modules/Management/Pages/Meat Sales/NewMeatSales'
import AllMeatSales from '@/components/Modules/Management/Pages/Meat Sales/AllMeatSales'
import SingleMeatSales from '@/components/Modules/Management/Pages/Meat Sales/SingleMeatSales'
import SingleMeatSaleButchery from '@/components/Modules/Management/Pages/Meat Sales/SingleMeatSaleButchery'

// Meat Types
import NewMeatType from '@/components/Modules/Management/Pages/Meat Types/NewMeatType'
import AllMeatTypes from '@/components/Modules/Management/Pages/Meat Types/AllMeatTypes'
import SingleMeatType from '@/components/Modules/Management/Pages/Meat Types/SingleMeatType'
import MeatTypeSettings from '@/components/Modules/Management/Pages/Meat Types/MeatTypeSettings'

// Menu
import NewMenu from '@/components/Modules/Management/Pages/Meal Menu/NewMenu'
import AllMenus from '@/components/Modules/Management/Pages/Meal Menu/AllMenus'
import SingleMenu from '@/components/Modules/Management/Pages/Meal Menu/SingleMenu'
import SingleMenuButchery from '@/components/Modules/Management/Pages/Meal Menu/SingleMenuButchery'
import MenuSettings from '@/components/Modules/Management/Pages/Meal Menu/MenuSettings'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/management',
      component: Management,
      children: [
        {
          path: '/dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        // Meat Purchase
        {
          path: 'NewMeatPurchase',
          name: 'NewMeatPurchase',
          component: NewMeatPurchase
        },
        {
          path: 'AllMeatPurchases',
          name: 'AllMeatPurchases',
          component: AllMeatPurchases
        },
        {
          path: 'MeatPurchaseSettings',
          name: 'MeatPurchaseSettings',
          component: MeatPurchaseSettings
        },
        {
          path: 'SingleMeatPurchase/:meat_purchase_id',
          name: 'SingleMeatPurchase',
          component: SingleMeatPurchase
        },
        {
          path: 'SingleMeatPurchaseButchery/:butchery_id',
          name: 'SingleMeatPurchaseButchery',
          component: SingleMeatPurchaseButchery
        },
        {
          path: 'SingleMeatPurchaseMeatType/:meat_type_id',
          name: 'SingleMeatPurchaseMeatType',
          component: SingleMeatPurchaseMeatType
        },
        // Meat Sales
        {
          path: 'NewMeatSales',
          name: 'NewMeatSales',
          component: NewMeatSales
        },
        {
          path: 'AllMeatSales',
          name: 'AllMeatSales',
          component: AllMeatSales
        },
        {
          path: 'SingleMeatSales/:meat_sale_id',
          name: 'SingleMeatSales',
          component: SingleMeatSales
        },
        {
          path: 'SingleMeatSaleButchery/:butchery_id',
          name: 'SingleMeatSaleButchery',
          component: SingleMeatSaleButchery
        },
        // Meat Types
        {
          path: 'NewMeatType',
          name: 'NewMeatType',
          component: NewMeatType
        },
        {
          path: 'AllMeatTypes',
          name: 'AllMeatTypes',
          component: AllMeatTypes
        },
        {
          path: 'SingleMeatType/:meat_type_id',
          name: 'SingleMeatType',
          component: SingleMeatType
        },
        {
          path: 'MeatTypeSettings',
          name: 'MeatTypeSettings',
          component: MeatTypeSettings
        },
        // Meal Menu
        {
          path: 'NewMenu',
          name: 'NewMenu',
          component: NewMenu
        },
        {
          path: 'AllMenus',
          name: 'AllMenus',
          component: AllMenus
        },
        {
          path: 'SingleMenu/:meal_id',
          name: 'SingleMenu',
          component: SingleMenu
        },
        {
          path: 'SingleMenuButchery/:butchery_id',
          name: 'SingleMenuButchery',
          component: SingleMenuButchery
        },
        {
          path: 'MenuSettings',
          name: 'MenuSettings',
          component: MenuSettings
        },
        // Meat Price
        {
          path: 'NewMeatPriceSale',
          name: 'NewMeatPriceSale',
          component: NewMeatPriceSale
        }
      ]
    }
  ],
  mode: 'history'
})
